/*Aggregate = cluster of things that are brough together
		= needs a form of information that is not visibly available from your documents*/


db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: ["Philippines", "US"]
	},
	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: ["Philippines", "Ecuador"]	
	},
	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: ["US", "China"]	
	},
	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: ["Philippines", "India"]	
	},
]);



// SECTION-MONGODB AGGREGATION
// used to generate manipulated data and perorm operations to create filtered results that help in analyzing data.


// USING THE AGGREGATE METHOD***************************
/*match = used to pass the documents that meet the specified condition/s to the next aggregate stage
$group = is used to group together and filed-value pairs uisng the data from the grouped elements
&sum= will total the values 

SYNTAX: 
db.collectionName.aggregate([
	{ $match: { fieldA, valueA} },
	{ $group: { _id: "$fieldB"}, {result: {operation}}}
])
*/


db.fruits.aggregate([
	{ $match: {onSale: true}},
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock"}}}
]);


// FIELD PROJECTION WITH AGGREGATION- exclude*********************
db.fruits.aggregate ([
	{ $match: {onSale: true}},
	{ $group: {_id: "supplier_id", total: { $sum: "$stock"}}},
	{ $project: {_id: 0}}	
]);


/* SORTING AGGREGATED RESULTS******************************

$sort - can be used to change the order of the aggregated results
*/

db.fruits.aggregate ([
	{ $match: { onSale: true } },
	{ $group: { _id: "supplier_id", total: { $sum: "$stock"} } },
	{ $sort: { total: -1} }	
]);



// AGGREGATING RESULTS BASED ON ARRAY FIELDS
/*
$unwind - deconstructs an array field from a collection with an array value to output a result for each element
SYNTAX:  
{$unwind: field}
*/

db.fruits.aggregate ([
	{ $unwind: "$origin"}
]);


db.fruits.aggregate ([
	{ $unwind: "$origin"},
	{ $group: { _id: "$origin", kinds: { $sum: 1} } }
]);






// OTHER AGGREGATE STAGES
// $avg
db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "color", yellow_fruits_stock: {$avg: "$stock"}}}
]);



// $count
db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $count: "Yellow Fruits"}
]);



// $min & max
db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "color", yellow_fruits_stock: {$min: "$stock"}}}
]);


db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "color", yellow_fruits_stock: {$max: "$stock"}}}
]);












